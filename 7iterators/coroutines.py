from functools import wraps
def coroutine(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen
    return wrapper

@coroutine
def grep(pattern):
    """Searcher for pattern"""
    print('Searching for ', pattern)
    result = -1
    while True:
        print(f'Before yield {result}')
        line = yield  result
        print(f'After yield {result}')
        result = line.find(pattern)

@coroutine
def seen_words():
    words = set()
    result = False
    while True:
        word = yield result
        if word in words:
            result = True
        else:
            result = False
            words.add(word)

# Має бути ф-я генератор ( gen_numbers), що генерує числа
# від 1 до 100. Ця ф-я має відправляти дані у even_filter
# ( генератор, що приймає дані) якщо число парне то відправити його
# у наступний фільтр.  filter_40_70 ( coroutine ), що
# приймає число і якщо воно в діапазоні від 40 до 70 відправляє
# його у file_coroutine . file_coroutine  - приймає числа
# і записує іх у файл final_numbers.txt, кожна число із
# нового рядка. ( таким чином має бути 1 генератор + 2 фільтри +
# 1 consumer для запису у файл).   Використати декоратор
# coroutine для виклику 1 раз next.
# gen_numbers -> even_filter -> filter_40_70 -> file_coroutine

@coroutine
def file_coroutine():
    while True:
        result = yield
        with open('results.txt', 'a') as fi:
            fi.write(str(result)+'\n')

@coroutine
def filter_40_70(file_coroutine):
    while True:
       var_input = yield
       if  71 > var_input > 40:
           file_coroutine.send(var_input)

@coroutine
def filter_even(filter_40_70):
    while True:
       var_input = yield
       if var_input % 2 == 0:
           filter_40_70.send(var_input)

def generate_numbers(filter_even):
    for x in range(1, 101):
        yield x
        filter_even.send(x)


if __name__=="__main__":

    file_cor = file_coroutine()
    fil_40_70 = filter_40_70(file_cor)
    fil_even = filter_even(fil_40_70)
    gen = generate_numbers(fil_even)
    list(gen)
    print(res)





