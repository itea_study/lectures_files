import random

class Entry:
    def __init__(self, key, value):
        self.key = key
        self.value = value
    def __str__(self):
        return f'{self.key}'

    def __eq__(self, other):
        if self.key == other.key:
            return True
        return False
    def __contains__(self, item):
        if self == item:
            return True
        if self.key > item.key:
            if hasattr(self, 'right'):
                return item in self.right
            return False
        else:
            if hasattr(self, 'left'):
                return item in self.left
            return False

class Tree:

    def __contains__(self, item):
        return item in self.root

    @classmethod
    def put(cls, key, value):
        if not hasattr(cls, 'root'):
            cls.root = Entry(key, value)
        else:
            entry = Entry(key, value)
            if not entry in cls.root:
                cls.secondPut(cls.root, entry)

    @classmethod
    def secondPut(cls, entry0, entry1):
        if entry0.key > entry1.key:
            if hasattr(entry0, 'right'):
                cls.secondPut(entry0.right, entry1)
            else:
                entry0.right = entry1
        else:
            if hasattr(entry0, 'left'):
                cls.secondPut(entry0.left, entry1)
            else:
                entry0.left = entry1
    @classmethod
    def print_tree(cls, entry, integer):
        print(' '*integer, entry)
        if hasattr(entry, 'left'):
            cls.print_tree(entry.left, integer+1)
        if hasattr(entry, 'right'):
            cls.print_tree(entry.right, integer+1)

tree = Tree()
for x in range(20):
    tree.put(random.randint(20, 40), 0)

tree.print_tree(tree.root, 0)
