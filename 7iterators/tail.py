import time
def follow(thefile):
    thefile.seek(0)
    while True:
         line = thefile.readline()
         if not line:
             time.sleep(1)
             continue
         yield line

with open('numbers.txt', 'r') as fi:
    gen = follow(fi)
    while True:
        print('calling next')
        res = next(gen)
        print(res)