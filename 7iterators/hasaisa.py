def a():
    class Wheel:
        def __init__(self):
            self.radius = 30

    class Car:
        def __init__(self, wheel):
            self.wheel = wheel

    wheel = Wheel()
    car = Car(wheel)

def b():
    class Wheel:
        def __init__(self):
            self.radius = 30

    class Car:
        def __init__(self):
            self.wheel = Wheel()

    car = Car()

# class Wheel:
#     def __init__(self, color):
#         self.radius = 30
#         self.color = color

