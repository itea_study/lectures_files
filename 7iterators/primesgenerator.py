def primes_generator(n=1000):
    count = 1 if n > 2 else 0
    if count == 0:
        yield 0
        return 'custom data for StopIteration'
    yield 2
    list = [True] * n
    for i in range(3, n, 2):
        if not list[i]:
            continue
        count += 1
        yield i
        for o in range(i * i, n, i):
            list[o] = False