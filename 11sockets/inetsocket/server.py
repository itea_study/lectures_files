import os
import socket


SOCKET_ADDR = ('localhost', 9091)
print("Відкриваємо INET сокет...")
server = socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM)
server.bind(SOCKET_ADDR)
print("Слухаємо...")
server.listen(1)
conn, addr = server.accept()
print("Під'єднався ", conn, addr)

while True:
    data = conn.recv(2)
    if data:
        print('Got 2 bytes')
        data = data.decode('UTF-8')
    print(data)
    if data == "NE":
        conn.close()
        break
print("-" * 20)
print("Закриваємо сокет...")
server.close()
print("Завершення")