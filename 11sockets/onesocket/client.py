import os
import socket

SOCKET_FILE = './test.socket'

print("З'єднання...")
if os.path.exists(SOCKET_FILE):
    client = socket.socket(socket.AF_UNIX,
                           socket.SOCK_DGRAM)
    client.connect(SOCKET_FILE)
    print("З'єднались.")
    print("Ctrl-C щоб вийти.")
    print("Відправити 'DONE' щоб вимкнути сервер")
    while True:
        try:
            x = input("> ")
            if x != '':
                print(f"Відправлено: {x}")
                client.send(x.encode('utf-8'))
                if x == "DONE":
                    print("Завершення.")
                    break
        except KeyboardInterrupt as k:
            print("Виключення.")
            break
    client.close()
else:
    print("Не можу з'єднатись!")
print("Виконано")
