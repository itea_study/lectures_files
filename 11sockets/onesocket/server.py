import os
import socket

SOCKET_FILE = './test.socket'

if os.path.exists(SOCKET_FILE):
    os.remove(SOCKET_FILE)

print("Відкриваємо UNIX сокет...")
server = socket.socket(socket.AF_UNIX,
                       socket.SOCK_DGRAM)
server.bind(SOCKET_FILE)

print("Слухаємо...")
while True:
    data = server.recv(1024)
    if not data:
        break
    else:
        print("-" * 20)
    print(data)
    if data == "DONE":
        break
print("-" * 20)
print("Закриваємо сокет...")
server.close()
os.remove(SOCKET_FILE)
print("Завершення")