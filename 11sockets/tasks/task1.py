# розпарсити сторінку https://lviv.itea.ua/courses-itea/python/python-advanced/
# 1.1) надрукувати title сторінки
# 1.2) надрукувати назви усіх занять

import requests
from pprint import pprint
from lxml import html
from bs4 import BeautifulSoup


def main():
    response = requests.get('https://lviv.itea.ua/courses-itea/python/python-advanced/')
    soup = BeautifulSoup(response.content, 'html.parser')
    print(soup.title.text)

    tree = html.fromstring(response.content)
    target_li = tree.xpath('//*[@id="contForSide"]/div[3]/div/ul/li/')
    li_text = [li.text.strip() for li in target_li]
    pprint(li_text)

    # all_li = soup.find_all('li')
    # li_text = [li.text.strip() for li in all_li]

main()
