import requests
from pprint import pprint
from lxml import html
from bs4 import BeautifulSoup


def main():
    domain = 'https://www.leboncoin.fr'
    response = requests.get(
        'https://www.leboncoin.fr/recherche/?category=10&regions=26&real_estate_type=2')

    pages_from_pagination = [
        'https://www.leboncoin.fr/recherche/?category=10&regions=26&real_estate_type=2']


    soup = BeautifulSoup(response.content, 'html.parser')
    urls = soup.find_all(attrs={'class': '_1f-eo'})
    hrefs = [url.href for url in urls]

    import ipdb;ipdb.set_trace()

    tree = html.fromstring(response.content)
    target_li = tree.xpath('//*[@id="contForSide"]/div[3]/div/ul/li/')
    li_text = [li.text.strip() for li in target_li]
    pprint(li_text)

    # all_li = soup.find_all('li')
    # li_text = [li.text.strip() for li in all_li]

main()
