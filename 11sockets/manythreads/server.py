import socket
from time import sleep
from threading import Thread
from queue import Queue

SHUTDOWN = False
queue = Queue(3)

class SocketThread(Thread):
    def __init__(self, connection):
        super().__init__()
        self.connection = connection
        self.connection.settimeout(5)
    def run(self):
        global SHUTDOWN, queue
        while not SHUTDOWN:
            try:
                data = self.connection.recv(1024)
            except:
                continue
            if data:
                data = data.decode('UTF-8')
            else:
                break
            print(data)
            if data == "DONE":
                SHUTDOWN = True
        self.connection.close()
        queue.get()


SOCKET_ADDR = ('localhost', 9091)
print("Відкриваємо INET сокет...")
server = socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM)
server.bind(SOCKET_ADDR)
print("Слухаємо...")
server.listen(3)
while not SHUTDOWN:
    if not queue.full():
        conn, addr = server.accept()
        print("Під'єднався ", addr)
        t = SocketThread(conn)
        queue.put(t)
        t.start()
    else:
        sleep(1)

print("-" * 20)
print("Закриваємо сокет...")
server.close()
print("Завершення")