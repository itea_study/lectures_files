import socket

SOCKET_ADDR = ('localhost', 9091)
print("З'єднання...")
client = socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM)
client.connect(SOCKET_ADDR)
print("З'єднались.\nCtrl-C щоб вийти.")
print("Відправити 'DONE' щоб вимкнути сервер")

while True:
    try:
        x = input("> ")
        if x != '':
            print(f"Відправлено: {x}")
            client.send(x.encode('utf-8'))
            if x == "DONE":
                print("Завершення.")
                break
    except KeyboardInterrupt as k:
        print("Виключення.")
        break
client.close()

print("Виконано")
