import socket
from threading import Thread

CONNECTION_LIST = []

class Broadcast:
    def __init__(self, conn_list):
        self.conn_list = conn_list

    def set_data(self, sock, message):
        if sock and message:
            for socket in self.conn_list[1:]:
                if socket != sock:
                    socket.send(message)


class ClientThread(Thread):
    def __init__(self, sock):
        super().__init__()
        self.sock = sock
        self.daemon = True

    def run(self):
        global CONNECTION_LIST
        while True:
            try:
                data = self.sock.recv(4096)
                if not data:
                    break
            except:
                broadcast.set_data(
                    self.sock, f"Client {addr} is offline")
                print("Client {addr} is offline")
                self.sock.close()
                CONNECTION_LIST.remove(self.sock)
                break

            data = data.decode('UTF-8')
            if data == "DONE":
                broadcast.set_data(self.sock, "Client quits".encode('UTF-8'))
                self.sock.close()
                CONNECTION_LIST.remove(self.sock)
                break
            elif data == "QUIT":
                broadcast.set_data(self.sock, "QUIT".encode('UTF-8'))
                self.sock.close()
                CONNECTION_LIST = []
                break
            else:
                broadcast.set_data(self.sock, data.encode('UTF-8'))


class IncomingThread(Thread):

    def __init__(self):
        super().__init__()
        self.server_socket = socket.socket(socket.AF_INET,
                                      socket.SOCK_STREAM)
        self.server_socket.bind(("127.0.0.1", 9090))
        self.server_socket.listen(10)
        # self.server_socket.setblocking(True)
        CONNECTION_LIST.append(self.server_socket)

    def run(self):
        while CONNECTION_LIST:
            try:
                self.server_socket.settimeout(0.1)
                sockfd, addr = self.server_socket.accept()
                CONNECTION_LIST.append(sockfd)
                client_thread = ClientThread(sockfd)
                client_thread.start()
                print(f"Client ({addr}) connected")
                broadcast.set_data(
                    sockfd, f"Client {addr} connected".encode('UTF-8'))
            except socket.timeout:
                pass


        self.server_socket.close()


if __name__ == "__main__":

    print("TCP/IP Chat server process started.")

    broadcast = Broadcast(CONNECTION_LIST)

    incoming_thread = IncomingThread()
    incoming_thread.start()
