import select
import socket
import sys

SOCKET_ADDR = ('127.0.0.1', 9090)
print("З'єднання...")
client = socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM)
client.connect(SOCKET_ADDR)
print("З'єднались.\nCtrl-C щоб вийти.")
print("Відправити 'DONE' щоб вимкнути сервер")

while True:
    try:
        i, o, e = select.select([sys.stdin], [], [], 0.1)
        if i:
            i = sys.stdin.readline().strip()
            print(f"Відправлено: {i}")
            client.send(i.encode('utf-8'))
            if i in ("DONE", "QUIT"):
                print("Завершення.")
                break
        client.settimeout(0.1)
        data = client.recv(1024)
        if data:
            data = data.decode('UTF-8')
            print(data)
            if data == 'QUIT':
                break
    except socket.timeout:
        pass

    except KeyboardInterrupt as k:
        print("Виключення.")
        break
client.close()

print("Виконано")
