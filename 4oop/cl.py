import inspect
import math


def get_class_that_defined_method(meth):
    return getattr(inspect.getmodule(meth), meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[0])

def static_method(func):
    def wrapper(*args, **kwargs):
        if len(args)>0:
            if not type(args[0]).__name__ in dir(__builtins__):
                return func(*args[1:], **kwargs)
        return func(*args, **kwargs)
    return wrapper

def class_method(func):
    def wrapper(*args, **kwargs):
        if len(args)>0:
            if not type(args[0]).__name__ in dir(__builtins__):
                return func(*(args[0].__class__,)+args[1:], **kwargs)
            return func(*(get_class_that_defined_method(func),)+args, **kwargs)
        return func(*args, **kwargs)
    return wrapper


class Pizza:
    def __init__(self, radius, height):
        self.radius = radius
        self.height = height

    @static_method # якщо не написати виклик через інстанс не можливий
    def compute_area(radius):
         print(radius)
         return math.pi * (radius ** 2)

    @class_method # якщо не написати то треба першим аргументом передати клас
    def compute_volume(cls, height, radius):
        print(cls.__name__)
        return height * cls.compute_area(radius)

    def get_volume(self):
        return self.compute_volume(self.height, self.radius)
pizza = Pizza(20, 30)
print('-'*30)
print(pizza.compute_volume(40, 30))
print(Pizza.compute_volume(40, 30))



