class Droid:
    counter = 0
    blocked_attr = ['name', 'counter']

    def __init__(self, name):
        self.__dict__['name'] = name
        self.__class__.counter +=1
        self.__dict__['counter'] = self.__class__.counter

    def __setattr__(self, key, value):
        if key in self.__class__.blocked_attr:
            print(f'{key} cannot be changed')
        else:
            self.__dict__[key] = value
    def __getattr__(self, key):
        if key not in self.__dict__:
            print(f'{key} no such key')
        else:
            return self.__dict__[key]
    def __delattr__(self, key):
        if key in self.__class__.blocked_attr:
            print(f'{key} cannot be deleted')
        else:
            self.__dict__.pop(key, None)

