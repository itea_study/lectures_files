import time
from flask import Flask, Response
from celery import Celery
# pip install celery

def make_celery(app):
    celery = Celery('fl', backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


app = Flask('customapplication')
app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379/1',
    CELERY_RESULT_BACKEND='redis://localhost:6379/1'
)
celery = make_celery(app)


@celery.task()
def hard():
    result = 0
    for x in range(10**9):
        result += x
        result += x
    return result

@app.route("/taras")
def hello1():
    #result.wait()
    return 'tarasvaskiv '


@app.route("/")
def hello():
    result = hard.delay()
    result.wait()
    return f'tarasvaskiv {result}'

if __name__=='__main__':
    app.run()
