from flask import (
    Flask, render_template,
    request, redirect, url_for, session as flask_session,
    send_from_directory
)
from celery import Celery
from models import Base, User, Department, Client, Application

import redis
import os
import cv2
import numpy as np
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


PWD = os.getcwd()
redis_server = redis.StrictRedis(
    host="localhost", port=6379,
    charset="utf-8", decode_responses=True, db=0)
app = Flask('customapplication')
app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379/1',
    CELERY_RESULT_BACKEND='redis://localhost:6379/1'
)

app.secret_key = 'secretkey'
engine = create_engine('sqlite:///bank.db')

Session = sessionmaker(bind=engine)

Base.metadata.create_all(engine)

def make_celery(app):
    celery = Celery('start', backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery
celery = make_celery(app)


@app.route('/')
def index_page():
   return render_template('index.html')


@app.route('/logout')
def logout():
   del flask_session['username']
   return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    session = Session()
    if request.method=='POST':
        email = request.form['email']
        password = request.form['password']
        user = session.query(User).filter(User.email == email).first()
        # user = User.objects.get(email=email).first()
        if user and user.password == password:
            flask_session['username'] = user.first_name
        return render_template('index.html')
    else:
        return render_template('login.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    session = Session()
    if request.method == 'POST':
        email = request.form['email']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        password = request.form['password']
        confirm_password = request.form['confirm_password']
        user = User(first_name=first_name, last_name=last_name, email=email, password=password)
        session.add(user)
        session.commit()
        return redirect(url_for('login'))
    else:
        return render_template('register.html')


@app.route('/departments', methods=['GET', 'POST'])
def departments():
    session = Session()
    if request.method == 'POST':
        city = request.form['city']
        count_of_workers = request.form['count_of_workers']
        department = Department(city=city,
                                count_of_workers=count_of_workers)
        session.add(department)
        session.commit()

    departments = session.query(Department).all()
    return render_template('departments.html',
                           departments=departments)


@app.route('/uploads/<path:filename>')
def download_file(filename):
    return send_from_directory('media', filename, as_attachment=True)


def mse(imageA, imageB):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err


@celery.task()
def process_image(filename):
    image_path = os.path.join(PWD, 'media', filename)

    new_image = cv2.imread(image_path)

    for image2 in os.listdir(os.path.join(PWD, 'media')):
        image2_path = os.path.join(PWD, 'media', image2)
        if new_image != image2_path:
            image = cv2.imread(image2_path)
            err = mse(new_image, image)
            print('err')
            print(err)
            if err < 5000:
                os.remove(image_path)
                return False
    return True


@app.route('/dashboard', methods=['POST', 'GET'])
def dashboard_page():
    if request.method == 'POST':

        file = request.files['file']
        image_path = os.path.join(PWD, 'media', file.filename)
        file.save(image_path)
        result = process_image.delay(file.filename)
        result.wait()
        print(result)
        session = Session()
        user = session.query(User).filter(
            User.first_name == flask_session['username']).first()
        if result.result:

            user.image = file.filename
            session.commit()
        return render_template('dashboard.html', user=user)
    else:
        session = Session()
        if 'username' in flask_session:
            user = session.query(User).filter(
                User.first_name == flask_session['username']).first()
            return render_template('dashboard.html', user=user)
        else:
            return 'Not logined'


if __name__=='__main__':
    app.run(debug=True)
