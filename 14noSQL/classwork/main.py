import csv

from models import (
    Department,
    Application, Client
)

from pymodm.connection import connect

connect("mongodb://localhost:27017/bank",
        alias="default")

departments = dict()
clients = dict()

def upload_from_csv_and_add_to_db():

    with open('department.csv', 'r') as c_file:
        reader = csv.reader(c_file)
        for n, row in enumerate(reader):
            if n==0:
                pass
            else:
                department = Department(city=row[1],
                                        count_of_workers=row[2])
                department.save()
                departments[row[0]] = department
    with open('client.csv', 'r') as c_file:
        reader = csv.reader(c_file)
        for n, row in enumerate(reader):
            if n==0:
                pass
            else:
                client = Client(first_name=row[1],
                                last_name=row[2],
                                education=row[3],
                                passport=row[4],
                                city=row[5],
                                age=row[6])
                department = departments[row[7]]
                department.clients.append(client)
                clients[row[0]] = client
    with open('application.csv', 'r') as c_file:
        reader = csv.reader(c_file)
        for n, row in enumerate(reader):
            if n==0:
                pass
            else:
                application = Application(
                    sum=row[1],
                    credit_state=row[2],
                    currency=row[3])
                clients[row[4]].applications.append(application)

    for v in departments.values():
        v.save()
# 1.    Вибрати усіх клієнтів, зі всіма данними які можливо.
# 2.    Вибрати львівські відділення банку.
# 3.    Порахувати кількість клієнтів усіх відділень та лише львівських відділень.
# 4.    Визначити кількість заявок на кредит для кожного клієнта.
# 5.    Вивести відділення, яке видало в кредити найбільше грошей
# 6.    Видалити усі кредити, які є повернені.

def custom_queries():

    print('1 all clients\n',)
    all_clients = 0
    all_lviv = 0
    all_deps = Department.objects.all()
    for dp in all_deps:
        all_clients += len(dp.clients)
        if dp.city == 'Lviv':
            all_lviv += len(dp.clients)
        for cl in dp.clients:
            print(cl)
    print('2 lviv departments \n')
    lv_deps = Department.objects.raw({'city': 'Lviv'}).all()
    for d in lv_deps:
        print(d)
    print('3 all clients, only lviv \n')
    print(f'{all_clients}  {all_lviv}')

    print('4 appl for all clients \n')
    for d in all_deps:
        for client in d.clients:
            print(client, len(client.applications))




if __name__=='__main__':
    # upload_from_csv_and_add_to_db()
    custom_queries()
