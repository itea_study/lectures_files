from xml.dom.minidom import parse


# Open XML document using minidom parser
DOMTree = parse("books.xml")
collection = DOMTree.documentElement


# Get all the books in the collection
books = collection.getElementsByTagName("book")

# Print detail of each book.
for book in books:
   print("*****Book*****")
   if book.hasAttribute("year"):
      print(f"Was written in:"
            f" {book.getAttribute('year')}")

   revision = book.getElementsByTagName('revision')[0]
   text = revision.getElementsByTagName('comment')[0].childNodes[0]
   print(f"Revision: text = {text.data}")
   id = revision.getElementsByTagName('id')[0].childNodes[0]
   print(f"Id: {id.data}")

   text = revision.getElementsByTagName('text')[0].childNodes[0]
   print(f"Text: {text.data}")