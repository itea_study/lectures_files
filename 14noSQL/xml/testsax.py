import xml.sax

class BookHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.CurrentData = ""
        self.title = ""
        self.id = ""
        self.revision = ""
        self.text = ""

    # Call when an element starts
    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "book":
            print("*****Movie*****")
            year = attributes["year"]
            print("Was written in:", year)

    # Call when an elements ends
    def endElement(self, tag):
        if self.CurrentData == "title":
            print("Title:", self.title)
        elif self.CurrentData == "id":
            print("Id:", self.id)
        elif self.CurrentData == "text":
            print("Text: ", self.text)
        elif self.CurrentData == "revision":
            print("Revision: ", self.revision)
        elif self.CurrentData == "comment":
            print("Comment:", self.comment)
        self.CurrentData = ""

    # Call when a character is read
    def characters(self, content):
        if self.CurrentData == "title":
            self.title = content
        elif self.CurrentData == "id":
            self.id = content
        elif self.CurrentData == "revision":
            self.revision = content
        elif self.CurrentData == "comment":
            self.comment = content
        elif self.CurrentData == "text":
            self.text = content


if __name__ == "__main__":
    parser = xml.sax.make_parser()
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    Handler = BookHandler()
    parser.setContentHandler(Handler)

    parser.parse("books.xml")