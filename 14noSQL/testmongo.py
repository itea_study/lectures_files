from datetime import datetime
from pprint import pprint
from pymongo import MongoClient

client = MongoClient('localhost', 27017)

# отримання певної бази
db = client.post_database
posts = db.posts

def set_up():
    post = {"author": "Mike",
            "text": "My first blog post!",
            "tags": ["mongodb", "python", "pymongo"],
            "date": datetime.now()}

    post_id = posts.insert_one(post).inserted_id

    print(db.list_collection_names())
    pprint(posts.find_one({"author": "Mike"}))

def query_and_update():
    d = datetime(2009, 11, 12, 12)
    for post in posts.find({"date": {"$gt": d}}).sort("author"):
        pprint(post)

    posts.update({'author': 'Mike'}, {'$set': {'text': 'Text is updated.'} })

    target_post = posts.find_one({'author': 'Mike'})
    import ipdb;ipdb.set_trace()
    pass




if __name__=='__main__':
    query_and_update()


