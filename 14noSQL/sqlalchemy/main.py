from models import User, Address

from pymodm.connection import connect

connect("mongodb://localhost:27017/userAddress",
        alias="default")


def set_up_database():
    user1 = User(name='Andrew', surname='Jones',
                 password='edspassword')
    user2 = User(name='Ted', surname='Oldman',
                 password='edspassword')

    user1.save()
    user2.save()


def add_addreses():
    user = User.objects.raw({'name': 'Ted'}).first()
    # pymodm.errors.ValidationError: {'addresses':
    #  ['Referenced Models must be saved to the database first.']}
    address = Address(email_address='newaddress@gmail.com')
    address.save()
    user.addresses = [address]
    user.save()

def filtering_with_joins():
    # find all users with addresses
    target = 'Andrew'
    iter_results = User.objects.raw({'name': target}).all()
    for user in iter_results:
        for address in user.addresses:
            print(user.name)
            print(address.email_address)
    target = 'Ted'
    iter_results = User.objects.raw({'name': target}).all()
    for user in iter_results:
        for address in user.addresses:
            print(user.name)
            print(address.email_address)


if __name__=="__main__":
    # set_up_database()
    # add_addreses()
    # filtering_with_joins()
    import ipdb;ipdb.set_trace()
    pass
