from pymodm import EmbeddedMongoModel,\
    MongoModel, fields

# embedded -> вбудований
class Address(MongoModel):

    email_address = fields.CharField()

    def __repr__(self):
        return "Address(email_address={})".format(
            self.email_address)


class User(MongoModel):

    name = fields.CharField()
    surname = fields.CharField()
    password = fields.CharField()

    addresses = fields.ListField(
        fields.ReferenceField(Address)
    )

    def __repr__(self):
        return "User(name={}, surname={}, password={})".format(
            self.name, self.surname, self.password)


