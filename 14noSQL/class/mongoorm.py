#  Перетворити ORM із relationalDBS  в ORM із використанням mongodb
#  Зробити такі селекти:
# 1.    Вибрати усіх клієнтів, зі всіма данними які можливо.
# 2.    Вибрати львівські відділення банку.
# 3.    Порахувати кількість клієнтів усіх відділень та лише львівських відділень.
# 4.      Визначити кількість заявок на кредит для кожного клієнта.
# 5.    Вивести відділення, яке видало в кредити найбільше грошей
# 6.    Видалити усі кредити, які є повернені.

import csv
from mongomodels import (
    Department,
    Application, Client
)
from pymodm.connection import connect

connect("mongodb://localhost:27017/bank",
        alias="default")

deps = dict()
clients = dict()
def upload_from_csv_and_add_to_db():

    with open('department.csv', 'r') as c_file:
        reader = csv.reader(c_file)
        for n, row in enumerate(reader):
            if n==0:
                pass
            else:
                department = Department(city=row[1],
                                        count_of_workers=row[2])
                deps[row[0]] = department
    with open('client.csv', 'r') as c_file:
        reader = csv.reader(c_file)
        for n, row in enumerate(reader):
            if n==0:
                pass
            else:
                client = Client(first_name=row[1],
                                last_name=row[2],
                                education=row[3],
                                passport=row[4],
                                city=row[5],
                                age=row[6])
                deps[row[7]].clients.append(client)
                clients[row[0]] = client

    with open('application.csv', 'r') as c_file:
        reader = csv.reader(c_file)
        for n, row in enumerate(reader):
            if n==0:
                pass
            else:
                application = Application(
                    sum=row[1],
                    credit_state=row[2],
                    currency=row[3])
                clients[row[4]].applications.append(application)

    for v in deps.values():
        v.save()


def custom_queries():
    departaments = Department.objects.all()
    for dep in departaments:
        for client in dep.clients:
            print(client)
    print('1\n',)


if __name__=='__main__':
    upload_from_csv_and_add_to_db()
    custom_queries()
