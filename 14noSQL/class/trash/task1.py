# Створіть англо-німецький словник за допомогою REDIS.
# Слова для словника: stork / storch, hawk / falke, woodpecker / specht
# і owl / eule. Виведіть німецький варіант слова owl. Додайте у словник,
# на ваш вибір, ще два слова та їхній переклад. Виведіть окремо: ключі і
# значення словника у вигляді списків. Видаліть усі ключі із бази redis.


import redis

redis_server = redis.StrictRedis(host="localhost",
                                 port=6379,
                                 charset="utf-8",
                                 decode_responses=True,
                                 db=0)


def task1():
    redis_server.set('stork', 'storch')
    redis_server.set('hawk', 'falke')
    redis_server.set('woodpecker', 'specht')
    redis_server.set('owl', 'eule')

    print(redis_server.get('owl'))
    redis_server.set('duck', 'ente')
    redis_server.set('chicken', 'duchchicken')
    keys = redis_server.keys()
    print(keys)
    values = []
    for key in keys:
        values.append(redis_server.get(key))
        redis_server.delete(key)
    print(values)
    print(f'After delete all keys {redis_server.keys()}')



if __name__=="__main__":
    task1()