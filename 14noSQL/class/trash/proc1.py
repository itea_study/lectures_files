# Написати  2 класи унаслідуватись від multiprocessing.
# Process.  Один процес читає веб-сторінку і кладе результат в redis.
# Інший процес  отримує результати із redis і записує значення ( response.content)
# у файл.

import redis
import requests

from time import time, sleep
from multiprocessing import Process

redis_server = redis.StrictRedis(host="localhost",
                                 port=6379,
                                 charset="utf-8",
                                 decode_responses=True,
                                 db=0)

class Producer(Process):

    def run(self):
        for x in range(10):
            response = requests.get('http://24tv.ua')
            redis_server.set(time(), response.content)
            sleep(10)


class Consumer(Process):

    def run(self):
        with open('results.txt', 'a') as target_file:
            for x in range(11):
                for key in redis_server.keys():
                    target_file.write(redis_server.get(key))
                    redis_server.delete(key)
                sleep(22)

if __name__=='__main__':
    p1 = Producer()
    # p2 = Consumer()
    p1.start()
    # p2.start()
    p1.join()
    # p2.join()
