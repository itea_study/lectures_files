from mysql import connector
config = {'host': 'localhost',
          'user': 'root',
          'password': '110889'}
def print_databases_tables(con):
    cursor = con.cursor()
    cursor.execute('show databases')
    for database in cursor:
        print(database)
    con.close()
    config['database'] = 'mysql'
    con = connector.connect(**config)
    cursor = con.cursor()
    cursor.execute('show tables')
    for table in cursor:
        print(table)

def create_database(con):
    cursor = con.cursor()
    cursor.execute('CREATE DATABASE TEST')
    cursor.execute('SHOW DATABASES')
    for database in cursor:
        print(database)
    cursor.close()
if __name__=='__main__':
    con = connector.connect(**config)
    print_databases_tables(con)
    del config['database']
    con = connector.connect(**config)
    create_database(con)
