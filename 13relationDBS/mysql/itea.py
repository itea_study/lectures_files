"""
Запис даних до таблиці(insert),
видалення даних(delete),
оновлення даних(update),
вибір усіх даних в таблиці(select).
Вибірка завдяки WHERE.
Оператори AND, OR, BETWEEN, LIKE('%'), DISTINCT, IN(аналог OR) та інші, пов’язані з вибіркою даних.
"""
"""
1) створити базу даних iteadatabase
2) створити таблицю iteastudents 
  ( id - первинни ключ,
    name - імя студента varchar(50) 
    surname - прізвище студента varchar(50)
    salary  - сума яку заробляє студент  int
3) додати записи у таблицю iteastudents такі записи
    sergiy kozlovskiy 1500
    petya videev 1800
    kolya anisimov 900
    dima zhuravlyov 700 
    sergiy shorobyra 2000
4) виконати наступні селекти із таблиці
 4.1) показати усю інформацію про всіх студентів
 4.2) показати лише імена всіх студентів
 4.3) показати імена та прізвища всіх студентів
 4.4) показати студента в якого id = 1
 4.5) показати студентів в яких id > 1
 4.6) показати студентів в яких id != 3
 4.7) показати студентів в яких імя kolya
 4.8) показати студентів в яких імя dima і зарплата менше 1000
 4.9) п-ти ст-ів в яких імя sergiy або  petya
 4.10) п-ти ст-ів в яких id у діапазоні від 2-5
 4.11) n-ти ст-ів в яких id або 2 або 5
 4.12) п-ти ст-ів в яких імя закінчується на а
 4.13) п-ти ст-ів в який імя починається на s
 4.14) п-ти ст-ів в яких в імені є буква l
 4.15) п-ти ст-ів в яких довжина імені 5 символів
 4.16) оновити таблицю. В кого id 1, 3, 4 встановити зарплату 5000
 4.17) видалити із таблиці в кого id 1, 2
 4.18) показати усіх студентів
 """
result = """
create database if not exists iteadatabase;
USE iteadatabase;

create table if not exists iteastudents
(
id int primary key auto_increment,
name varchar(50),
surname varchar(50),
salary int
);

insert into iteastudents(name,surname ,salary)
values
('sergiy','kozlovskiy',1500),
('petya','videev',1800),
('kolya','anisimov',900),
('dima','zhuravlyov',700),
('sergiy','shorobyra',2000);

select * from iteastudents;
select name from iteastudents;
select name,surname from iteastudents;
select * from iteastudents where id =1;
select * from iteastudents where id >1;
select * from iteastudents where id <> 3;/*те саме що і наступне*/
select * from iteastudents where id != 3;
select * from iteastudents where name = 'petya';
select * from iteastudents where name = 'dima' and salary <1000;
select * from iteastudents where name = 'dima' or salary <1000;
select * from iteastudents where name = 'dima' or name = 'petya';
select * from iteastudents where id between 2 and 5;
select * from iteastudents where id  in (2,5);
select * from iteastudents where name like ('%a');
select * from iteastudents where name like('s%');
select * from iteastudents where name like('%e%');
select * from iteastudents where name like ('serg_y');
select * from iteastudents where name like ('_____');
select * from iteastudents where name like ('%_');

select distinct * from iteastudents;

update iteastudents
set salary = 5000
where id in (2,5,7);


delete from iteastudents where id in (2,5,7);
select * from iteastudents;
set sql_safe_updates = 0;
delete name from iteastudents where id = 1;
"""







