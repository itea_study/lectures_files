from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, User, Address

engine = create_engine('sqlite:///database.db')

Session = sessionmaker(bind=engine)
session = Session()

def create_all_tables():
    Base.metadata.create_all(engine)

def set_up_database():
    user1 = User(name='Andrew', surname='Jones',
                 password='edspassword')
    user2 = User(name='Ted', surname='Oldman',
                 password='edspassword')

    session.add(user1)
    session.add(user2)

    queried_user = \
        session.query(User).filter_by(name='Ted').first()
    print(queried_user)
    session.commit()


def add_addreses():
    user = session.query(User).filter_by(name='Ted').first()
    user.addresses = [Address(
        email_address='newaddress@gmail.com')]
    session.add(user)
    session.commit()

def filtering_with_joins():
    target = 'Andrew'
    iter_results = session.query(User, Address).filter(
        User.id==Address.user_id
    ).filter(User.name==target).all()
    for user, address in iter_results:
        print(user.name)
        print(address.email_address)
    target = 'Ted'
    iter_results = session.query(User, Address).filter(
        User.id == Address.user_id
    ).filter(User.name == target).all()
    for user, address in iter_results:
        print(user.name)
        print(address.email_address)


if __name__=="__main__":
    create_all_tables()
    # set_up_database()
    # add_addreses()
    filtering_with_joins()

