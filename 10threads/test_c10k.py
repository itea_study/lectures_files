from time import time
from threading import Thread
import requests
def time_decorator(func):
    def wrapper(*args, **kwargs):
        start = time()
        func(*args, **kwargs)
        print(time() - start)
    return wrapper



def a():
    with open('results_a.txt', 'a') as a_file:
        for x in range(50):
            response = requests.get(
                'https://lviv.itea.ua/courses-itea/python/python-advanced/')
            a_file.write('')

def b():
    with open('results_b.txt', 'a') as b_file:
        for x in range(50):
            response = requests.get('https://ecotown.com.ua/')
            b_file.write(str(response.status_code))

@time_decorator
def no_threads():
    a()
    b()

@time_decorator
def with_threads():
    t1 = Thread(target=a)
    t2 = Thread(target=b)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
if __name__=="__main__":
    with_threads()