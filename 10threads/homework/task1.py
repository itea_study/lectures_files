#  Використити  об’єкт синхронізації condition.
#  Написати 2 потоки унаслідуватись від Thread.
#  Один потік читає веб-сторінку і кладе результат в чергу.
#  Інший потік отримує результати із черги і записує у файл.
# Використати  AlmostAlwaysTrue , засікти час виконання коду.
import requests
from threading import Thread, Condition
from queue import Queue
from utils import time_decorator, AlmostAlwaysTrue

queue = Queue()
condition = Condition()  # lock


class ProducerThread(Thread):

    def __init__(self, url):
        super().__init__()
        self.url = url

    def run(self):
        true_var = AlmostAlwaysTrue(10)
        while true_var:
            response = requests.get(self.url)
            condition.acquire()
            queue.put(response)
            print('put into queue')
            condition.notify()
            condition.release()


class ConsumerThread(Thread):

    def __init__(self):
        super().__init__()

    def run(self):
        true_var = AlmostAlwaysTrue(20)
        with open('results.txt', 'a') as fi:
            while true_var:
                condition.acquire()
                if queue.empty():
                    condition.wait()
                    print('awaked')
                response = queue.get()
                condition.release()
                fi.write(str(response.content)+'\n')

@time_decorator
def main():
    t0 = ProducerThread('http://24tv.ua')
    t1 = ConsumerThread()
    t0.start()
    t1.start()
    t0.join()
    t1.join()

if __name__=='__main__':
    main()

