from time import time


def time_decorator(func):
    def wrapper(*args, **kwargs):
        start = time()
        func(*args, **kwargs)
        print(time() - start)
    return wrapper

class AlmostAlwaysTrue:
    def __init__(self, counter):
        self.start = 0
        self.counter = counter

    def __bool__(self):
        if self.start < self.counter:
            self.start += 1
            return True
        return False