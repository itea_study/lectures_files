import numpy as np
from time import time

def mse(imageA, imageB):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err

def time_decorator(func):
    def wrapper(*args, **kwargs):
        start = time()
        func(*args, **kwargs)
        print(time() - start)
    return wrapper