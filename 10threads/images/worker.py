import argparse
import cv2
import os

from utils import mse


def process_images(path, images):
    results = []
    for image in images:
        new_image = cv2.imread(f'{path}/new/{image}')
        for image2 in os.listdir(f'{path}/old'):
            old_image = cv2.imread(f'{path}/old/{image2}')
            err = mse(new_image, old_image)
            results.append(err)
    # print(results)


def main():
    parser = argparse.ArgumentParser(description='--Worker--')
    parser.add_argument('all_images_path', type=str,
                        help='Directory of all images')
    parser.add_argument('images', metavar='N', type=str, nargs='+',
                        help='List of images to be processed')
    args = parser.parse_args()
    process_images(args.all_images_path, args.images)

if __name__=='__main__':
    main()