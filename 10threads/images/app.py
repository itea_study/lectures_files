import os
import multiprocessing

from shutil import copyfile
from subprocess import Popen
from time import sleep
from utils import time_decorator



PWD = os.path.dirname(os.path.realpath(__file__))

def prepare_images():
    os.mkdir(f'{PWD}/old')
    os.mkdir(f'{PWD}/new')
    #old images
    for x in range(300):
        copyfile(f'{PWD}/jobs0.jpg', f'{PWD}/old/{x}jobs0.jpg')
        copyfile(f'{PWD}/jobs1.jpg', f'{PWD}/old/{x}jobs1.jpg')
        copyfile(f'{PWD}/jobs2.jpg', f'{PWD}/old/{x}jobs2.jpg')
    # new images
    for x in range(4):
        copyfile(f'{PWD}/jobs0.jpg', f'{PWD}/new/{x}jobs0.jpg')
        copyfile(f'{PWD}/jobs1.jpg', f'{PWD}/new/{x}jobs1.jpg')
        copyfile(f'{PWD}/jobs2.jpg', f'{PWD}/new/{x}jobs2.jpg')

@time_decorator
def two_processes():
    images = os.listdir(f'{PWD}/new')

    first_process = ['python', 'worker.py', f'{PWD}']
    for x in images[:6]:
        first_process.append(x)
    p0 = Popen(first_process)

    sec_process = ['python', 'worker.py', f'{PWD}']
    for x in images[6:]:
        sec_process.append(x)
    p1 = Popen(sec_process)
    while True:
        if p0.poll() != None  \
                and p1.poll() != None:
            break


@time_decorator
def one_process():
    images = os.listdir(f'{PWD}/new')

    first_process = ['python', 'worker.py', f'{PWD}']
    for x in images:
        first_process.append(x)
    p0 = Popen(first_process)

    while True:
        if p0.poll() != None:
            break

@time_decorator
def cpu_processes():
    images = os.listdir(f'{PWD}/new')
    processes = []
    for number in range(multiprocessing.cpu_count()):
        process_args = ['python', 'worker.py', f'{PWD}']
        for x in images[number::multiprocessing.cpu_count()]:
            process_args.append(x)
        p = Popen(process_args)
        processes.append(p)


    while True:
        if not any([process for process in processes
                    if process.poll() == None]):
            break




def main():
    if not os.path.exists(f'{PWD}/old'):
        prepare_images()
    cpu_processes()
    two_processes()
    one_process()

if __name__=="__main__":
    main()