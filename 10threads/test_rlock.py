from threading import Thread, RLock, Lock
lock = RLock()

def a():
    with lock:
        print('inside a')
        b()

def b():
    with lock:
        print('inside b')

def c():
    with lock:
        print('inside c')


if __name__=='__main__':
    t1 = Thread(target=a)
    t2 = Thread(target=c)
    t1.start()
    t2.start()
    t1.join()
    t2.join()