from threading import Thread
from time import sleep, time

def a():
    def simulate_cpu_job(time):
        print('Before job a')
        sleep(time)
        print('After job a')

    thread = Thread(target=simulate_cpu_job, name='a-Thread',
                    args=(10,))
    thread.start()
    return thread

def b():
    class CustomThread(Thread):
        def __init__(self, time):
            super().__init__()
            self.name = 'CustomThread'
            self.time = time

        def run(self):
            print('Before job b')
            sleep(self.time)
            print('After job b')

    ct = CustomThread(10)
    ct.start()
    return ct
res = [a(), b()]



# results = dict()
# def sum1():
#     result = 0
#     for x in range(1, 10**7):
#         result += x
#     results['sum1'] = result
# def sum2():
#     result = 0
#     for x in range(10 ** 7, 10 ** 8):
#         result += x
#     results['sum2'] = result
# def time_decorator(func):
#     def wrapper(*args, **kwargs):
#         start = time()
#         func(*args, **kwargs)
#         print(time() - start)
#     return wrapper
# @time_decorator
# def no_threads():
#     sum1()
#     sum2()
#     print(results['sum1'], ' ', results['sum2'])
# # no_threads()
#
# @time_decorator
# def with_threads():
#     t1 = Thread(target=sum1)
#     t2 = Thread(target=sum2)
#     (t1.start(), t2.start())
#     t1.join()
#     t2.join()
#     print(results['sum1'], ' ', results['sum2'])
# with_threads()





