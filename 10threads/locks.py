from threading import Thread, RLock
import requests
from test_c10k import time_decorator
lock = RLock()
def a():
    for x in range(50):
        with lock:
            fi.write('Function a\n')
            response = requests.get(
                'https://lviv.itea.ua/courses-itea/python/python-advanced/')
            fi.write(f'status code for a = {"".join(str(response.content).split())}\n')
def b():
    for x in range(50):
        with lock:
            fi.write('Function b\n')
            response = requests.get('https://ecotown.com.ua/')
            fi.write(f'status code for b = {"".join(str(response.content).split())}\n')
@time_decorator
def with_threads():
    t1 = Thread(target=a)
    t2 = Thread(target=b)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
if __name__=='__main__':
    fi = open('results1.txt', 'a')
    with_threads()
    fi.close()