from queue import Queue
from random import randint
from threading import Condition, Thread

queue = Queue()
condition = Condition()
final_result = 0


class AlmostAlwaysTrue:
    def __init__(self, counter):
        self.start = 0
        self.counter = counter

    def __bool__(self):
        if self.start < self.counter:
            self.start += 1
            return True
        return False



def producer_thread():
    custom_true = AlmostAlwaysTrue(10)
    while custom_true:
        condition.acquire()
        queue.put(randint(10, 20))
        condition.notify()
        condition.release()

def consumer_thread():
    custom_true = AlmostAlwaysTrue(10)
    while custom_true:
        condition.acquire()
        while not queue:
            condition.wait()

        global final_result
        final_result += queue.get()
        condition.release()

t1 = Thread(target=producer_thread)
t2 = Thread(target=consumer_thread)
t1.start()
t2.start()
t1.join()
t2.join()
print(final_result)
