from tkinter import *

class Calculator:
    def __init__(self, root):
        # Buttons:
        zeroButton = Button(root, text='0', width=20, height=3,
                            bg='LightBlue', fg='red', command=lambda: self.add_sybol('0')).place(x=17, y=382)
        oneButton = Button(root, text='1', width=8, height=3,
                           bg='LightBlue', fg='red', command=lambda: self.add_sybol('1')).place(x=17, y=302)
        twoButton = Button(root, text='2', width=8, height=3,
                           bg='LightBlue', fg='red', command=lambda: self.add_sybol('2')).place(x=100, y=302)
        threeButton = Button(root, text='3', width=8, height=3,
                             bg='LightBlue', fg='red', command=lambda: self.add_sybol('3')).place(x=182, y=302)
        fourButton = Button(root, text='4', width=8, height=3,
                            bg='LightBlue', fg='red', command=lambda: self.add_sybol('4')).place(x=17, y=222)
        fiveButton = Button(root, text='5', width=8, height=3,
                            bg='LightBlue', fg='red', command=lambda: self.add_sybol('5')).place(x=100, y=222)
        sixButton = Button(root, text='6', width=8, height=3,
                           bg='LightBlue', fg='red', command=lambda: self.add_sybol('6')).place(x=182, y=222)
        sevenButton = Button(root, text='7', width=8, height=3,
                             bg='LightBlue', fg='red', command=lambda: self.add_sybol('7')).place(x=17, y=142)
        eightButton = Button(root, text='8', width=8, height=3,
                             bg='LightBlue', fg='red', command=lambda: self.add_sybol('8')).place(x=100, y=142)
        ninthButton = Button(root, text='9', width=8, height=3,
                             bg='LightBlue', fg='red', command=lambda: self.add_sybol('9')).place(x=182, y=142)

        # Main entry.
        self.num1 = StringVar()
        self.txtDisplay = Entry(root, textvariable=self.num1, relief=RIDGE,
                           bd=10, width=33, insertwidth=1, font=40)
        self.txtDisplay.place(x=15, y=10)
        self.txtDisplay.focus()

        decimalButton = Button(root, text='.', width=8, height=3, bg='powder blue',
                               command=lambda: self.add_sybol('.')).place(x=182, y=382)
        equalButton = Button(root, text='=', width=8, height=8, bg='Lightgreen',
                             command=self.make_cacl).place(x=264, y=302)
        plusButton = Button(root, text='+', width=8, height=3, bg='gray',
                            command=lambda: self.add_sybol('+')).place(x=264, y=222)
        minusButton = Button(root, text='-', width=8, height=3, bg='gray',
                             command=lambda: self.add_sybol('-')).place(x=264, y=142)
        multiplyButton = Button(root, text='x', width=8, height=3, bg='gray',
                                command=lambda: self.add_sybol('*')).place(x=264, y=66)
        divideButton = Button(root, text='÷', width=8, height=3, bg='gray',
                              command=lambda: self.add_sybol('/')).place(x=182, y=66)
        clearButton = Button(root, text='Clear (CE)', width=20, height=3,
                             command=self.clear, bg='Orange').place(x=17, y=66)

        # Locks the parent windows size.
        root.maxsize(370, 450)
        root.minsize(370, 450)

    def clear(self):
        self.txtDisplay.delete(0, END)

    def add_sybol(self, symbol):
        old_text = self.num1.get()
        self.num1.set(old_text + symbol)

    def make_cacl(self):
        result = eval(self.num1.get())
        self.num1.set(str(result))


if __name__=='__main__':
    #Parent Window.
    root = Tk()
    calc = Calculator(root)
    root.title('Calculator ++ [1.7.2]')
    root.geometry('370x550')

    #Parent window's background color:
    root.configure(background = 'black')
    root.mainloop()