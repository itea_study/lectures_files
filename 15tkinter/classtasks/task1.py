from tkinter import *

class Task1:
    def __init__(self, root):

        self.entryText = StringVar(root)
        self.entryText.set('Make input')

        textEntry = Entry(root, textvariable=self.entryText)
        textEntry.pack()

        btn = Button(root, text='Copy text', command=self.text_copy)
        btn.pack()

        self.lbl = Label(root, text='Empty temporarily')
        self.lbl.pack(side='bottom')

    def text_copy(self):
        user_input = self.entryText.get()
        self.lbl['text'] = user_input
        self.entryText.set('')


if __name__=='__main__':

    root = Tk()
    root.geometry('300x200')
    root.title('A simple application')
    task = Task1(root)
    root.mainloop()