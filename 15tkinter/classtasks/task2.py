from tkinter import *

class Task1:
    def __init__(self, root):

        frame1 = Frame(root)
        frame1.pack(fill='x')
        Label(frame1, text='A', bg='red').pack(fill='x')

        frame3 = Frame(root)
        frame3.pack(side='bottom')
        Label(frame3, text='D', bg='yellow', width=15).pack(side='bottom')

        frame2 = Frame(root)
        frame2.pack(fill='x', side='bottom')
        Label(frame2, text='B', bg='blue', width=15).pack(side='left', expand=TRUE)
        Label(frame2, text='C', bg='white', width=15).pack(side='right')




if __name__=='__main__':

    root = Tk()
    root.geometry('300x90')
    root.title('A simple application')
    task = Task1(root)
    root.mainloop()