from tkinter import *
from random import randint, choice
from math import sqrt, pow


class Circle:
    colors = ['blue', 'red', 'green', 'yellow']
    def __init__(self, root, canvas):
        self.x1 = 20
        self.y1 = 20
        self.x2 = 60
        self.y2 = 60
        self.canvas = canvas
        self.circle_id = canvas.create_oval(self.x1, self.y1,
                                            self.x2, self.y2, fill=choice(self.colors))
        self.get_incrementation()

        self.canvas.after(100, self.update_coordinates)

    @property
    def center(self):
        return self.x1 + self.radius, self.y1 + self.radius

    @property
    def radius(self):
        return (self.x2-self.x1)/2


    def get_incrementation(self):
        self.incrementx = randint(-5, 5)
        self.incrementy = randint(-5, 5)

    def handle_collision(self):
        if self.x1 < 0 or self.x2 > 400:
            self.incrementx = -self.incrementx
        if self.y1 < 0 or self.y2 > 400:
            self.incrementy = -self.incrementy

        self_center = self.center
        for circle in circles:
            if circle != self:
                circle_center = circle.center
                distance = sqrt(
                    pow(self_center[0] - circle_center[0], 2) +
                    pow(self_center[1] - circle_center[1], 2)
                    )
                if distance >= self.radius + circle.radius:
                   circle.incrementx = - circle.incrementx
                   self.incrementx = - self.incrementx
                   circle.incrementy = - circle.incrementy
                   self.incrementy = - self.incrementy

    def update_coordinates(self):

        self.x1 += self.incrementx
        self.x2 += self.incrementx

        self.y1 += self.incrementy
        self.y2 += self.incrementy
        self.handle_collision()

        self.canvas.coords(self.circle_id, self.x1, self.y1,
                           self.x2, self.y2)
        self.canvas.after(10, self.update_coordinates)


if __name__ == '__main__':
    root = Tk()
    root.geometry("400x400")
    root.title('Demonstrating after event')

    canvas = Canvas(root, width=400, height=400, borderwidth=0,
                    highlightthickness=0, bg="white")
    canvas.grid()
    circle1 = Circle(root, canvas)
    circle2 = Circle(root, canvas)
    circle3 = Circle(root, canvas)
    global circles
    circles = [circle1, circle2, circle3]
    root.mainloop()