from tkinter import *
def f1():

    # створення кореня
    # має бути в кожному проекті
    root = Tk()

    root.title('A simple application')
    root.mainloop()


from tkinter import *

def f2():
    def quit():
        import sys
        sys.exit()
    root = Tk()
    lbl = Label(root, text="Press the button below to exit")
    lbl.pack()
    btn = Button(root, text="Quit", command=quit)
    btn.pack()

    root.mainloop()


def f3():
    class Example:
        def __init__(self, master):
            lbl = Label(master, text="Press the button below to exit")
            lbl.pack()
            btn = Button(master, text="Quit", command=quit)
            btn.pack()

        def quit(self):
            import sys
            sys.exit()



