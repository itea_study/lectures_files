from tkinter import *

x1 = 20
y1 = 20
x2 = 90
y2 = 90
width = 400
height = 400

incrx = 15
incry = 5


def change(canvas, circle):
    global x1, y1, x2, y2, incrx, incry
    # x1, y1, x2, y2 = x1+incrx, y1+incrx, x2+incrx, y2+incrx
    x1, y1, x2, y2 = x1+incrx, y1+incry, x2+incrx, y2+incry
    if x1 < 0:
        incrx = -incrx
    if x2 > height:
        incrx = -incrx
    if y1 < 0:
        incry = -incry
    if y2 > width:
        incry = -incry

    canvas.coords(circle, x1, y1, x2, y2)
    canvas.after(1000, change, canvas, circle)


def mouse_press(event):
    print('Mouse pressed')


if __name__ == '__main__':
    root = Tk()
    root.geometry(f"{width}x{height}")
    root.title('Demonstrating after event')

    canvas = Canvas(root, width=width, height=height, borderwidth=0,
                    highlightthickness=0, bg="white")
    canvas.grid()

    root.bind("<Button-1>", mouse_press)

    circle1 = canvas.create_oval(x1, y1, x2, y2, fill='blue')

    canvas.after(1000, change, canvas, circle1)
    root.mainloop()