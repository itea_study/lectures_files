from tkinter import *

class Example2:
    def __init__(self, master):
        self.master = master

        frame = Frame(master)
        self.headLbl = Label(frame, text='Widget demo',
                             relief=RIDGE)
        self.headLbl.pack(side=TOP, fill=X)

        spacerFrame = Frame(frame, borderwidth=10)
        centerFrame = Frame(spacerFrame)
        leftCol = Frame(centerFrame, relief=GROOVE,
                        borderwidth=2)
        rightCol = Frame(centerFrame, relief=GROOVE,
                         borderwidth=2)

        self.colorLbl = Label(rightCol, text='Select a color')
        self.colorLbl.pack(expand=YES, fill=X)

        entryText = StringVar(master)
        entryText.set('Select a color')
        self.colorEntry = Entry(rightCol, textvariable=entryText)
        self.colorEntry.pack(expand=YES, fill=X)

        self.radioBtns = []
        self.radioVal = StringVar(master)
        btnList = ('black', 'red', 'green', 'blue', 'white',
                   'yellow')
        for color in btnList:
            self.radioBtns.append(Radiobutton(leftCol, text=color, value=color, indicator=TRUE,
                                              variable=self.radioVal, command=self.updateColor))
        else:
            if (len(btnList) > 0):
                self.radioVal.set(btnList[0])
                self.updateColor()
        for btn in self.radioBtns:
            btn.pack(anchor=W)

        leftCol.pack(side=LEFT, expand=YES, fill=Y)
        rightCol.pack(side=LEFT, expand=YES, fill=BOTH)
        centerFrame.pack(side=TOP, expand=YES, fill=BOTH)

        self.indicVal = BooleanVar(master)
        self.indicVal.set(TRUE)
        self.updateIndic()
        Checkbutton(spacerFrame, text='Show indicator', command=self.updateIndic,
                    variable=self.indicVal).pack(side=TOP, fill=X)

        self.colorprevVal = BooleanVar(master)
        self.colorprevVal.set(FALSE)
        self.updateColorPrev()
        Checkbutton(spacerFrame, text='ColorProwiew', command=self.updateColorPrev,
                    variable=self.colorprevVal).pack(side=TOP, fill=X)

        Button(spacerFrame, text='Info', command=self.showInfo).pack(side=TOP, fill=X)
        Button(spacerFrame, text='Quit', command=self.quit).pack(side=TOP, fill=X)

        spacerFrame.pack(expand=YES, fill=BOTH)
        frame.pack(expand=YES, fill=BOTH)

    def quit(self):
        import sys;
        sys.exit()

    def updateColor(self):
        self.colorLbl.configure(fg=self.radioVal.get())
        self.colorEntry.configure(fg=self.radioVal.get())

    def updateIndic(self):
        for btn in self.radioBtns:
            btn.configure(indicatoron=self.indicVal.get())

    def updateColorPrev(self):
        if (self.colorprevVal.get()):
            for btn in self.radioBtns:
                color = btn.cget('text')
                btn.configure(fg=color)
        else:
            for btn in self.radioBtns:
                btn.configure(fg='black')

    def showInfo(self):
        toplevel = Toplevel(self.master, bg='white')
        toplevel.transient(self.master)
        toplevel.title('Program info')
        Label(toplevel, text='Simple tk demo', bg='white').pack(pady=20)
        Label(toplevel, text='Written by Bruno Bufor', bg='white').pack()
        Button(toplevel, text='Close', command=toplevel.withdraw).pack(pady=30)


if __name__ == '__main__':
    root = Tk()
    root.title('Simple demo')
    ex = Example2(root)
    root.mainloop()