class UpperAttrMetaclass(type):

    def __new__(cls, clsname, bases, dct):

        uppercase_attr = {}
        for name, val in dct.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val

        return type.__new__(cls, clsname, bases, uppercase_attr)

class CoolClass(metaclass=UpperAttrMetaclass):
    age = 40
    location = 'lviv'

print([(x, getattr(CoolClass, x)) for x in CoolClass.__dict__
       if not x.startswith('__')])
print(CoolClass().__dict__)