from time import time, sleep
cached_dict = dict()

def cached(*dec_args, **dec_kwargs):
    def real_decorator(func):
        def wrapper(*args, **kwargs):
            if args not in cached_dict:
                cached_dict[args] = {'time': time(),
                                     'res': func(*args, **kwargs)}
            else:
                if time() - cached_dict[args]['time'] > dec_args[0]:
                    cached_dict[args] = {'time': time(),
                                         'res': func(*args, **kwargs)}
            return cached_dict[args]['res']
        return wrapper
    return real_decorator

class cached_with_class:

    def __init__(self, *dec_args, **dec_kwargs):
        self.dec_args = dec_args
        self.dec_kwargs = dec_kwargs

    def __call__(self, method, *args, **kwargs):
        self._method = method
        self.args = args
        self.kwargs = kwargs
        return self
    def __get__(self, instance, owner):
        if self.args not in cached_dict:
            cached_dict[self.args] = {'time': time(),
                                      'res': func(self.args, self.kwargs)}
        # else:
        #     if time() - cached_dict[self.args]['time'] > dec_args[0]:
        #         cached_dict[args] = {'time': time(),
        #                              'res': func(*args, **kwargs)}
        return cached_dict[args]['res']


@cached_with_class(10)
def add(*args, **kwargs):
    print(f'Add was called with {args}')
    return sum(args)

print(add(1, 4, 6))
print(add(1, 4, 6))
print(add(1, 4, 6))

