from abc import ABC, abstractmethod
from math import pow

class Figure(ABC):
    @abstractmethod
    def square(self):
        pass

class Cube(Figure):
    def __init__(self, a):
        self.a = a
    def square(self):
        return self.a ** 2 * 6
    def get_volume(self):
        return self.a ** 3
    def set_volume(self, value):
        self.a = pow(value, 1.0/3)
    volume = property(get_volume, set_volume)
