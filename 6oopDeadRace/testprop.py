from abc import ABC, abstractmethod
from math import pow

class Figure(ABC):
    @abstractmethod
    def square(self):
        pass
class Cube(Figure):
    def __init__(self, a):
        self.a = a
    def square(self):
        return self.a ** 2 * 6
    @property
    def volume(self):
        return self.a ** 3
    @volume.setter
    def volume(self, value):
        assert value > 0
        self.a = pow(value, 1/3)
