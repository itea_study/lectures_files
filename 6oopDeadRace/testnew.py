def singleton(cls):
    instances = {}
    def getinstance(*args, **kwargs):
        print('Inside decorator')
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return getinstance

@singleton
class Droid:
    def __init__(self, name, age):
        print('Inside init')
        self.name = name
        self.age = age
    def __str__(self):
        return f'name= {self.name} age= {self.age}'
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)


dr = Droid('t', 3)
dr1 = Droid('t', 3)
print(dr)
print(dr1)