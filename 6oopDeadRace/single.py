def singleton(cls):
    instances = {}
    print('Only once')
    def getinstance(*args, **kwargs):
        print('Every time')
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return getinstance

@singleton
class DatabaseConnection:

    def __init__(self, path):
        import os
        self.file = open(os.getcwd() + path, 'r+')

    def __del__(self):
        print('File is deleted')
        self.file.close()
dc = DatabaseConnection('/testing.txt')
dc2 = DatabaseConnection('/testing.txt')
print(id(dc), id(dc2))