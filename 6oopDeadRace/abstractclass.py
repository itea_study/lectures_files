from abc import ABC, abstractmethod
from math import pow

class IntDescriptor:
    def __get__(self, instance, owner): # owner is class
        print('Inside get')             # of instance
        if 'd' not in instance.__dict__:
            print('d not in dict')
            instance.__dict__['d'] = 500
        return instance.__dict__['d']

class ParalelepipedCube(Figure):
    a = IntDescriptor()
    b = IntDescriptor()
    c = IntDescriptor()
    d = IntDescriptor()
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
    def square(self):
        return self.a * self.b * self.c

class Figure(ABC):

    def __init__(self):
        print('Init Figure class')
    @abstractmethod
    def square(self):
        pass
#
# class ABSMeta(type):
#     def __new__(mcls, name, bases, namespace, **kwargs):
#         cls = super().__new__(mcls, name, bases, namespace, **kwargs)
#         # Compute set of abstract method names
#         abstracts = {'squaree'}
#         cls.__abstractmethods__ = frozenset(abstracts)
#         return cls
# class ABS(metaclass=ABSMeta):
#     pass
# class Figure(ABC):
#     @abstractmethod
#     def square(self):
#         pass
class Cube(Figure):
    def __init__(self, a):
        self.a = a
    def square(self):
        return self.a ** 2

    def get_volume(self):
        return self.a **3
    def set_volume(self, value):
        self.a = pow(value, 1/3)
    def del_volume(self):
        print('No logic just deleting')
    volume = property(get_volume, set_volume,
                      del_volume,
                      doc='Property for volume')

cu = Cube(5)
print(cu.volume)
del cu.volume
print(Cube.volume.__doc__)
