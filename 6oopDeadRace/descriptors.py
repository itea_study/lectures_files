class IntDescriptor:
    def __get__(self, instance, owner):
        return self._value
    def __set__(self, instance, value):
        assert isinstance(value, int) \
               and value > 0
        self._value = value
    def __delete__(self, instance):
        del self._value

