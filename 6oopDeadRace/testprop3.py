from abc import ABC, abstractmethod
from descriptors import IntDescriptor
from math import pow

class Figure(ABC):
    @abstractmethod
    def square(self):
        pass
class Cube(Figure):
    def __init__(self, a):
        self.a = a
    @property
    def a(self):
        return self._a
    @a.setter
    def a(self, value):
        assert value > 0
        self._a = value
    @a.deleter
    def a(self):
        del self._a

    def square(self):
        return self._a ** 2 * 6
    @property
    def volume(self):
        return self._a ** 3
    @volume.setter
    def volume(self, value):
        assert value > 0
        self._a = pow(value, 1/3)
class RectangularParallelepiped(Figure):
    a = IntDescriptor()
    b = IntDescriptor()
    c = IntDescriptor()
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)
    def __init__(self, a, b, c):
        print('Inside __init__')
        self.a, self.b, self.c = a, b, c
    def square(self):
        return (self.a + self.b + self.c) * 2
    @property
    def volume(self):
        return self.a * self.b * self.c

rp = RectangularParallelepiped(1, 2, 3)
