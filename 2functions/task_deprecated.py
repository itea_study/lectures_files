# Написати 2 функції add ( додає позиційні аргументи в циклі
# for і вертає результат) і add_advance( яка додає позиційні
# аргументи використовуючи builtin функцію sum). Написати декоратор
# deprecated_add, обгорнути функцію add. У враппері натомість викликати
# функцію add_advanced замість add, і друкувати користувачеві повідомлення,
# що add функція є deprecated. Перевірити чи дійсно
# викликається функція add_advance.


def deprecated_add(func):
    def wrapper(*args, **kwargs):
        print(f'{func.__name__} is deprecated sorry !')
        return add_advance(*args, **kwargs)
    return wrapper

@deprecated_add
def add(*args, **kwargs):
    print('Add is called')
    res = 0
    for x in args:
        res += x
    return res

def add_advance(*args, **kwargs):
    print('Add advance is called')
    return sum(args)

if __name__=='__main__':
    print(add(1, 3, 5))