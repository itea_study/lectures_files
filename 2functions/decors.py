# Напишіть декоратор ( функцію logg), яка обертає функції.
# Суть декоратора  в тому, що він логує (записує у файл) позиційні
# і ключова аргументи. В кінці враппера записати у файл ‘Arguments
# are written\n’.  Також створити дві функції add & multiply,
# які підповідно додають, перемножують позиційні аргументи і
# вертають результат. Обернути функції add & multiply декоратором
# logg і викликати їх через if __name__ == “__main__”.
# Перевірити лог файл чи декоратор спрацював.

def logg(func):
    def wrapper(*args, **kwargs):
        fil = open('logs.txt', 'a')
        fil.write(str(args)+'\n')
        fil.write('Arguments are written\n')
        fil.close()
        return func(*args, **kwargs)
    return wrapper

@logg
def multiply(*args, **kwargs):
    result = 1
    for x in args:
        result = result * x
    return result

# Скопіювати функцію add із попереднього прикладу.
# Написати декоратор cached. Суть декоратора в кешуванні роботи
# функції add. Тобто треба створити словник де ключами будуть аргументи
# ( позиційні ) а значенням буде результат виконання функції add.
# Відповідно декоратор має перевіряти чи є дані в словнику(була вже викликана
# функція add із такими аргументами)  і вернути результат.
# Програмно перевірити чи дані беруться із кешу чи виконується функція add?

cached_dict = dict()

def cached(func):
    def wrapper(*args, **kwargs):
        if args not in cached_dict:
            cached_dict[args] = func(*args, **kwargs)
        return cached_dict[args]
    return wrapper

@cached
def add(*args, **kwargs):
    print(f'Add was called with {args}')
    return sum(args)

# Створити лямбда функцію, що множить число на рандом
# від 1 до 10. Згенерувати список від 1 до 10. Застосувити
# лямда функцію до цього списку, відфільтрувати цей список
# ( позбутись непарних чисел) і знайти суму списку. ( Використати map, reduce, filter )

import random
from functools import reduce

def map_reduce_filter():
    lambda_temp = lambda x: x * random.randint(1, 11)
    li = list(range(1, 11))
    print(li)
    li = map(lambda_temp, li)
    print(li)
    li = filter(lambda x: True if x % 2 == 0 else False, li)
    print(li)
    li = reduce(lambda x, y: x + y, li)
    return li

if __name__=='__main__':
    print(map_reduce_filter())

