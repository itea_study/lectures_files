
def multiply(func):
    def wraper():
        print('Before multiply was called')
        result = func() * 5
        print('After multiply was called')
        return result
    return wraper

def add(func):
    def wraper():
        print('Before add was called')
        result = func() + 5
        print('After add was called')
        return result
    return wraper


@multiply
@add
def number():
    return 100

print(number())