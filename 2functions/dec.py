def add(*dec_args, **dec_kwargs):
    def real_decorator(func):
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs) + dec_args[0]
        return wrapper
    return real_decorator


def add1(func, *args, **kwargs):
    def wrapper(*arg, **kwarg):
        return func(*args, **kwargs)
    return wrapper

@add(100, 200)
def func_num(a, b):
    return 10
print(func_num(10, 20))