def countPrimes(n):
    """
    Return number of prime numbers
    less than given n.

    :type n: int
    :rtype: int

    >>> countPrimes(10)
    4
    >>> countPrimes(12)
    5
    """
    count = 1 if n > 2 else 0
    if count == 0:
        return 0
    list = [True] * n
    for i in range(3, n, 2):
        if not list[i]:
            continue
        count += 1
        for o in range(i * i, n, i):
            list[o] = False
    return count