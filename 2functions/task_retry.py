# Написати функцію( gen_random) яка генерує рандомне число від 1 до 10.
# Якщо число = 1, то вернути його, якщо більше 1 то викликати помилку (ImportError).
# Написати функцію run ( яка буде викликати функцію gen_random
# і друкувати результат.  Створити декоратор retry, який буде 20
# разів пробувати викликати функцію gen_random. Якщо за 20 разів gen_random
# не вернув результат то вернути ‘NO RESULT’.
# Якщо помилки не було то вернути результат (1). Запустити програму.

import random

def retry(*dec_args, **dec_kwargs):
    def real_decorator(func):
        def wrapper(*args, **kwargs):
            for _ in range(dec_args[0]):
                try:
                    res = func(args[0])
                except ImportError as e:
                    continue
                else:
                    return res
            return 'NO RESULT'
        return wrapper
    return real_decorator

@retry(10)
def gen_random(a):
    result = random.randint(1, a)
    if result == 1:
        return result
    else:
        raise ImportError()

def run():
    print(gen_random(5))

if __name__=='__main__':
    run()



