from collections import deque

def main():
    de = deque()
    n = input()
    for x in range(int(n)):
        com = input().split()
        f = getattr(de, com.pop(0))
        if com:
            f(*com)
        else:
            f()
    print(' '.join([x for x in de]))

if __name__=='__main__':
    main()