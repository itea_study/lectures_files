import math
import os
import random
import re
import sys
from datetime import datetime
# Complete the time_delta function below.
def time_delta(t1, t2):
    # Sun 10 May 2015 13:54:36 -0700
    t1 = datetime.strptime(t1, '%a %d %b %Y %H:%M:%S %z')
    t2 = datetime.strptime(t2, '%a %d %b %Y %H:%M:%S %z')
    return int((t1-t2).total_seconds())

if __name__ == '__main__':

    t1 = 'Sat 02 May 2015 19:54:36 +0530'

    t2 = 'Fri 01 May 2015 13:54:36 -0000'

    delta = time_delta(t1, t2)

    print(delta)