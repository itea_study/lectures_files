# Д-р Джон Уеслі має електронну таблицю, що містить список
# IDs, name, class, makrs студентів.
# Ваше завдання полягає в тому, щоб допомогти доктору Уеслі
# обчислити середні оцінки студентів.
# Avarage = Sum of all marks / Total students
#        Треба використати namedtuple
# * Ідентифікатори, оцінки, клас і ім'я можуть бути записані
# в будь-якому порядку в електронній таблиці.
# * Надрукувати результат із заокругленням до 2 знаків після коми ( Decimal )
# 5
# ID         MARKS      NAME       CLASS
# 1          97         Raymond    7
# 2          50         Steven     4
# 3          91         Adrian     9
# 4          72         Stewart    5
# 5          80         Peter      6

from decimal import *
from collections import namedtuple


def main():
    n = int(input())
    headers = input()
    Student = namedtuple('St', headers)

    sum_of_marks = Decimal(0)
    for _ in range(n):
        stud = Student(*(input().split()))
        sum_of_marks += Decimal(stud.MARKS)
    result = sum_of_marks / n

    print(result.quantize(Decimal('0.01')))

if __name__=='__main__':
    main()
