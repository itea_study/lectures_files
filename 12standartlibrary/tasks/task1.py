# У вас є записи студентів. Кожен запис містить ім'я студента
# та оцінку у математиці, фізиці та хімії. Оцінки можуть бути
# плаваючими значеннями. Користувач вводить ціле число (к-сть студентів),
# за якими йдуть імена та оцінки для студентів. Ви повинні зберегти дані
# у словнику типу даних. Після цього користувач вводить ім'я студента.
# Виведіть середню оцінку, студента, округлення до 2 знаків.
from decimal import *

# getcontext().prec = 2


def main():
    num_of_st = input('Enter stud number')
    stud_marks = dict()

    for x in range(int(num_of_st)):
        user_info = input('Enter user marks').split()
        stud_marks[user_info[0]] = user_info[1:]

    target_name = input('Enter user name')
    import ipdb;ipdb.set_trace()
    result = sum([Decimal(x) for x in stud_marks[target_name]])/3
    print(result.quantize(Decimal('0.01')))

if __name__=='__main__':
    main()


