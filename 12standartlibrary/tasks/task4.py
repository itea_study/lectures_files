# Виконати операції  append, pop, popleft & appendleft над пустим  deque.
# Перший рядок містить ціле число - кількість операцій.
# Наступні рядки містять розділен пробілами,  назви методів та їх значенн.
# Треба надрукувати все що буде в deque після відповідних операцій.  Приклад input
# Output  має бути  '1 2'     (1 2 )
# 6
# append 1
# append 2
# append 3       ['append', '3'] -> ['3']
# appendleft 4
# pop
# popleft
from collections import deque

def main():
    dec = deque()

    n = int(input())
    for _ in range(n):
        opers = input().split()
        f = getattr(dec, opers.pop(0))
        f(*[int(x) for x in opers])

    print(' '.join([str(x) for x in dec]))


if __name__=='__main__':
    main()
