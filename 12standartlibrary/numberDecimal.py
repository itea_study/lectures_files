if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    q = input()
    from decimal import Decimal
    print(Decimal(sum(student_marks[q])/len(student_marks[q])).quantize(Decimal('0.01')))