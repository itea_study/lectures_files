from collections import namedtuple
from decimal import *

def main():
    n = input()
    Stud = namedtuple('Stud', ' '.join(input().split()))
    studs = []
    for x in range(int(n)):
        studs.append(Stud(*(input().split())))
    res = Decimal(sum([float(st.MARKS) for st in studs])/len(studs)).quantize(Decimal('0.01'))
    print(res)

def main1():
    n = 'MARKS      CLASS      NAME       ID*'\
    '92         2          Calum      1*'\
    '82         5          Scott      2*'\
    '94         2          Jason      3*'\
    '55         8          Glenn      4*'\
    '82         2          Fergus     5'
    inps = n.split('*')
    Stud = namedtuple('Stud', ' '.join(inps[0].split()))
    studs = []
    for x in range(5):
        studs.append(Stud(*(inps[x+1].split())))
    res = sum([float(st.MARKS) for st in studs]) / len(studs)
    print(Decimal(res).quantize(Decimal('0.01')))

if __name__=='__main__':
    main1()