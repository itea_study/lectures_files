import re
import csv
from lxml import html
import scrapy
import requests
import threading
from queue import Queue

my_dict = dict()

q = Queue()

mail_pattern = re.compile('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')

with open('/home/coder/PycharmProjects/info.csv', newline='', encoding='ISO-8859-1') as  c_file:
    c_reader = csv.reader(c_file, delimiter=',')
    for row in c_reader:
        url = row[45]
        if url:
            if url.startswith('http'):
                my_dict[row[45]] = {'emails':{}, 'staff': [], 'numbers': []}
            else:
                my_dict['http://'+row[45]] = {'emails': {}, 'staff': [], 'numbers': []}


def find_contact_a(response):

    # here find main page a tags
    tree = html.fromstring(response.content)
    main_a_hrefs = set(tree.xpath('//a/@href'))
    main_a_hrefs.discard('/')
    filtered_main_page_a_tags = set()

    for a in main_a_hrefs:
        curr_a = ''
        if a.startswith('http'):
            if a.startswith(response.url):
                curr_a = a
        else:
            curr_a = response.url + a
        filtered_main_page_a_tags.add(curr_a)

    contact_a_tags = [x for x in filtered_main_page_a_tags if 'ontact' in x]
    if contact_a_tags:
        return contact_a_tags[0]
    else:
        for x in filtered_main_page_a_tags:
            if x:
                filtered_main_page_a_tags.union(get_second_portion_of_a(x))
        contact_a_tags = [x for x in filtered_main_page_a_tags if 'ontact' in x]
        if contact_a_tags:
            return contact_a_tags[0]
        else:
            return ''


def get_second_portion_of_a(url):
    tree = html.fromstring(requests.get(url).content)
    main_a_hrefs = set(tree.xpath('//a/@href'))
    filtered_main_page_a_tags = set()

    for a in main_a_hrefs:
        curr_a = ''
        if a.startswith('http'):
            if a.startswith(url[:15]):
                curr_a = a
        else:
            curr_a = url + a
        filtered_main_page_a_tags.add(curr_a)
    return filtered_main_page_a_tags


def find_whos_who_a(response):
    # here find main page a tags
    tree = html.fromstring(response.content)
    main_a_hrefs = set(tree.xpath('//a/@href'))
    main_a_hrefs.discard('/')
    filtered_main_page_a_tags = set()

    for a in main_a_hrefs:
        curr_a = ''
        if a.startswith('http'):
            if a.startswith(response.url):
                curr_a = a
        else:
            curr_a = response.url + a
        filtered_main_page_a_tags.add(curr_a)

    contact_a_tags = [x for x in filtered_main_page_a_tags if 'whos' in x]
    if contact_a_tags:
        return contact_a_tags[0]
    else:#if no whos in
        staff_information = [x for x in filtered_main_page_a_tags if 'staff' in x]
        if staff_information:
            return staff_information[0]
        return ''


def get_info(site, response):
    contact_a = find_contact_a(response)
    whos_who_a = find_whos_who_a(response)

    # if whos_who_a:
    #     whos_who_page_content_raw = str(requests.get(whos_who_a).content)
    #     tree = html.fromstring(whos_who_page_content_raw)
    #     text = tree.xpath('//text()')
    #     text = [x.strip() for x in text if len(x) > 9]
    #     result_staff = set()
    #     for x in text:
    #         if 'eacher' in x:
    #             if len(x) < 20:
    #                 result_staff.add(x+text[text.index(x)+1])
    #             else:
    #                 result_staff.add(x)
    #     my_dict[x]['staff'] = ', '.join([x.strip() for x in result_staff if len(x)< 50])
    global mail_pattern
    s_emails = set(re.findall(mail_pattern, str(response.content)))

    if contact_a:
        # mails
        contact_page_content_raw = str(requests.get(contact_a).content)
        mail_pattern = re.compile('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')
        contact_s_emails = set(re.findall(mail_pattern, contact_page_content_raw))
        mails = s_emails.union(contact_s_emails)
        my_dict[site]['emails'] = ', '.join(mails)

        # staff
        if not  my_dict[site]['staff']:
            tree = html.fromstring(contact_page_content_raw)
            text = tree.xpath('//text()')
            result_staff = set()
            for x in text:
                if 'eacher' in x:
                    result_staff.add(x)
            my_dict[site]['staff'] = ', '.join([x.strip() for x in result_staff if len(x)< 50])

        # mobileles
        m_patrern = re.compile('[\d\s]{10,25}')
        mobiles = re.findall(m_patrern, contact_page_content_raw)
        # good_mobiles = [x for x in mobiles if x.strip()]
        good_mobiles = list(filter(lambda x: x.strip().startswith('20') or x.strip().startswith('020'), mobiles))
        if good_mobiles:
            my_dict[site]['numbers'] = ''.join(good_mobiles[0].split('&nbsp;')).strip()
    else:
        pass

    print(my_dict[site])



# for x in list(my_dict.keys()):
#     try:
#         resp = requests.get(x, timeout=10)
#         if resp.status_code != 200:
#             my_dict[x] = {'emails': {resp.status_code}, 'staff': [resp.status_code], 'numbers': [resp.status_code]}
#             continue
#         get_info(x, resp)
#     except ConnectionError:
#         my_dict[x] = {'emails': {'No connection'}, 'staff': ['No connection'], 'numbers': ['No connection']}
#     except Exception as e:
#         print('exception ', e)


for x in list(my_dict.keys()):
    q.put(x)
def func(x):
    try:
        resp = requests.get(x, timeout=10)
        if resp.status_code != 200:
            my_dict[x] = {'emails': {resp.status_code}, 'staff': [resp.status_code], 'numbers': [resp.status_code]}

        get_info(x, resp)
    except ConnectionError:
        my_dict[x] = {'emails': {'No connection'}, 'staff': ['No connection'], 'numbers': ['No connection']}
    except Exception as e:
        print('exception ', e)
def worker():
    while True:
        item = q.get()
        func(item)
        q.task_done()

for i in range(15):
    t = threading.Thread(target=worker)
    t.daemon = True
    t.start()
q.join()
	
	
file = open('/home/coder/PycharmProjects/results.csv', 'w', newline='', encoding='utf-8')
file_writer = csv.writer(file, delimiter=',')
file_writer.writerow(['site','emails','staff','numbers'])
for key in my_dict.keys():
    file_writer.writerow([str(key), str(my_dict[key]['emails']),
                          str(my_dict[key]['staff']),str(my_dict[key]['numbers'])])