import csv
from random import choice, randint


def prepare_data():
    global data
    data = dict()
    sex = ['Mr', 'Ms']
    names_mr = ['Oleg', 'Ivan', '']
    names_ms = ['Maria', 'Solomia', '']
    for x in range(100):
        data[x] = {'id': x, 'sex': choice(sex)}
        data[x]['name'] = choice(names_mr) \
            if data[x]['sex']=='Mr' else choice(names_ms)
        data[x]['age'] = randint(20, 50)


def write_data_to_file():
    with open('data.csv', 'w', newline='', encoding='utf-8') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(['id', 'sex', 'name', 'age'])
        for key, value in data.items():
            csv_writer.writerow([value['id'], value['sex'],
                                 value['name'], value['age']])


def filter_and_print_data():
    """filter women younger than 25"""
    with open('data.csv', newline='', encoding='utf-8') as  csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for number, row in enumerate(csv_reader):
            if number == 0:
                print(row)
            else:
                if row[1] == 'Ms' and int(row[3]) < 25:
                    print(row)

if __name__=="__main__":
    #prepare_data()
    #write_data_to_file()
    filter_and_print_data()