# Написати функцію, яка створить
# файл numbers.txt, якщо його не існує.
# Запишіть у файл 10 чисел, кожне з нового рядка
# ( згенерувати модулем random від 1 до 100).
# Напишіть функцію, яка зчитує ці числа з файлу і обчислює їх суму,
# виводить цю суму на екран і, водночас, записує цю суму у
# інший файл під назвою sum_numbers.txt

import random

def task1():
    with open('numbers.txt', 'x') as fi:
        for x in range(10):
            fi.write(str(random.randint(1, 100))+'\n')

def task_read1():
    with open('numbers.txt', 'r') as fi, open('sum.txt', 'w') as fi2:
        result = fi.readlines()
        result_int = map(lambda x: int(x), result)
        summa = sum(result_int)
        fi2.write(str(summa))

if __name__=='__main__':
    task1()
    task_read1()