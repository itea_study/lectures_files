def printing(a ):
    if isinstance(a, int):
        for x in range(a):
            print(x + 5)
    elif isinstance(a, str):
        for x in a:
            print(x )

if __name__ == "__main__":
    printing(4)
    printing('itea' )

