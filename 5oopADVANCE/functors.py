class Strip:
    def __init__(self, characters):
        self.characters = characters
    def __call__(self, string):
        return string.strip(self.characters)

strip_punctuation = Strip(",;:.!?")
print(strip_punctuation("Land ahoy!"))

# використання замикання, щоб зберег. стан
def strip_wrapper(characters):
    def wrapper(str_var):
        return str_var.strip(characters)
    return wrapper

strip_punct = strip_wrapper(",;:.!?")
print(strip_punct("Land ahoy!"))
