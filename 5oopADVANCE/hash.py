def a():
    class Droid:
        def __init__(self, name, age):
            self.name = name
            self.age = age

    army = set()
    dr = Droid('PQ-43', 30)
    army.add(dr)
    dr2 = Droid('PQ-43', 30)
    print(dr2 in army)
def b():
    class Droid:
        def __init__(self, name, age):
            self.name = name
            self.age = age
        def __hash__(self):
            return hash(self.name) + hash(self.age)
        def __eq__(self, other):
            return self.age == other.age and \
                   self.name == other.name
    army = set()
    dr = Droid('PQ-43', 30)
    army.add(dr)
    dr2 = Droid('PQ-43', 30)
    print(dr2 in army)
    dr3 = Droid('PQ-44', 30)
    print(dr3 in army)
a()
print('-'*30)
b()