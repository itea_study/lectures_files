import pickle

class Droid:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def __str__(self):
        return f'name= {self.name} age= {self.age}'
    def __hash__(self):
        return hash(self.name) + hash(self.age)
    def __eq__(self, other):
        return self.age == other.age and \
               self.name == other.name
drs = []
for x in range(5):
    dr = Droid(f'PQ-4{x}', 300+x)
    drs.append(dr)
with open('pickleDroids.pickle', 'wb') as pickle_file:
    pickle.dump(drs, pickle_file, pickle.HIGHEST_PROTOCOL)