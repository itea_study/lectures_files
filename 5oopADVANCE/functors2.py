class Droid:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def __str__(self):
        return f'name= {self.name} age= {self.age}'
class Sorting:
    def __init__(self, *attribute_names):
        self.attribute_names = attribute_names
    def __call__(self, instance):
        values = []
        for attribute_name in self.attribute_names:
            values.append(getattr(instance, attribute_name))
        return values
drs = []
for x in range(5):
    dr = Droid(f'PQ-4{x}', 300-x)
    drs.append(dr)
drs.sort(key=Sorting('name'))
print([str(x) for x in drs])
drs.sort(key=Sorting('age'))
print([str(x) for x in drs])