from datetime import datetime
import json

class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return {'__datetime__': o.replace(microsecond=0).isoformat()}
        return {'__{}__'.format(o.__class__.__name__): o.__dict__}

class CustomDecoder:
    def decode_object(o):
        if '__Droid__' in o:
            dr = Droid(o['__Droid__']['name'],
                       o['__Droid__']['age'])
            return dr
        elif '__datetime__' in o:
            return datetime.strptime(o['__datetime__'],
                                     '%Y-%m-%dT%H:%M:%S')
        return o

class Droid:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def __str__(self):
        return f'name= {self.name} age= {self.age}'
    def __hash__(self):
        return hash(self.name) + hash(self.age)
    def __eq__(self, other):
        return self.age == other.age and \
               self.name == other.name

with open('serializes.json', 'r') as json_file:
    dr = json.load(json_file, object_hook=CustomDecoder.decode_object)