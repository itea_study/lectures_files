from copy import deepcopy, copy
class Droid:
    def __init__(self, name, age, weapons):
        self.name = name
        self.age = age
        self.weapons = weapons
    def __str__(self):
        return f'name= {self.name} age= {self.age}'
    def __hash__(self):
        return hash(self.name) + hash(self.age)
    def __eq__(self, other):
        return self.age == other.age and \
               self.name == other.name
    def __add__(self, other):
        return Droid(f'{self.name} {other.name}',
                     self.age+other.age)

dr0 = Droid('PQ-43', 300, {'guns': ['ak-47', 'makar_pistol']})
dr1 = deepcopy(dr0)
dr2 = copy(dr0)

print(id(dr0.name)) #
print(id(dr1.name)) #
print(id(dr2.name)) #
