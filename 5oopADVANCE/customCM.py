from contextlib import contextmanager

cache_dict = dict()

def cached(func):
    def wrapper(*args, **kwargs):
        if args in cache_dict:
            print('Got from cache')
            return cache_dict[args]
        else:
            result = func(*args, **kwargs)
            cache_dict[args] = result
            print(f'We wrote in cache {args}')
            return result
    return wrapper

@cached
def countPrimes(n):
    count = 1 if n > 2 else 0
    if count == 0:
        return 0
    list = [True] * n
    for i in range(3, n, 2):  # parni vidkudaemo
        if not list[i]:
            continue
        count += 1
        for o in range(i * i, n, i):
            list[o] = False
    return count

@contextmanager
def clear():
    print('Simulationg __enter__')
    yield
    print('Clearing cache_dict')
    global cache_dict
    cache_dict = dict()

with clear() as cm:
    countPrimes(10**6)
    countPrimes(10**6)
countPrimes(10**6)