class File:
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        self.file = open(self.filename, self.mode)
        return self.file

    def __exit__(self, *args):
        print('File was closed')
        self.file.close()

for _ in range(5):
    with File('foo.txt', 'a') as infile:
        infile.write('foo')

