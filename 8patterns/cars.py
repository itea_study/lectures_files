from abc import ABC, abstractmethod

class Wheel:
    @abstractmethod
    def __init__(self): pass

    def __str__(self):
        return f' {self.radius} '

class BMWWheel(Wheel):
    """BMW Wheel"""
    def __init__(self):
        self.radius = 30

class MercedesWheel(Wheel):
    """Mercedes Wheel"""
    def __init__(self):
        self.radius = 40

class Door:
    @abstractmethod
    def __init__(self): pass

    def __str__(self):
        return f' {self.number} '

class BMWDoor(Door):
    """BMW Wheel"""
    def __init__(self):
        self.number = 4

class MercedesDoor(Door):
    """Mercedes Wheel"""
    def __init__(self):
        self.number = 5


class CarFactory(ABC):
    @abstractmethod
    def create_car(self): pass

class BMWFactory(CarFactory):
    def create_car(self):
        return [BMWWheel(), BMWDoor()]

class MercedesFactory(CarFactory):
    def create_car(self):
        return [MercedesWheel(), MercedesDoor()]

class CarShop:

    def __init__(self, factory):
        self.car_factory = factory

    def produce_car(self):
        car_parts = self.car_factory.create_car()
        print([str(x) for x in car_parts])

if __name__=='__main__':
    mf = MercedesFactory()
    cs = CarShop(mf)
    cs.produce_car()
    bf = BMWFactory()
    cs = CarShop(bf)
    cs.produce_car()

