class AbstractDb(ABC):

    def __init__(self, config):
        self.config = config

        self._backend = None
        self._db_name = None
        self._port = None
        self._host = None

    def get(self, key):
        return self.db.get(key)

    def put(self, key, value):
        self.set_value(key, value)

    def has(self, key):
        return self.has_value(key)

class RedisDb(AbstractDb):
    def __init__(self, config):
        super().__init__(config)
        import redis
        self._backend = "redis"
        self._host = self.config.get('cache_host')
        self._port = self.config.get('cache_port') or 6379
        self._db_name = self.config.get('cache_db_name') or 0
        self.db = redis.StrictRedis(host=self._host, port=self._port,
                                    db=self._db_name)
        self.set_value = self.db.set
        self.has_value = self.db.exists

class LazyDb(AbstractDb):
    def __init__(self, config):
        super().__init__(config)
        from lazydb import Db
        self.config = config
        self._backend = "lazydb"
        self._db_name = self.config.get('cache_db_name') or 'databridge_cache_db'
        self.db = Db(self._db_name)
        self.set_value = self.db.put
        self.has_value = self.db.has

class DBFactory(object):
    def __call__(self, config):
        return eval(config['db'])(config)

config = {'db': 'RedisDb'}
Db = DBFactory()
redis_db = Db(config)
