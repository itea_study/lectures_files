from abc import ABC, abstractmethod
from random import randint
class IObserver(ABC):
    @abstractmethod
    def update(self): pass

class RiskyPlayer(IObserver):
    name = 'RiskyPlayer'

    def update(self, fight):
        if fight.ABoxerScore > fight.BBoxerScore:
            print(f' {self.name} bet on BBoxer')
        else:
            print(f' {self.name} bet on ABoxer')

class ConservativePlayer(IObserver):
    name = 'ConservativePlayer'

    def update(self, fight):
        if fight.ABoxerScore < fight.BBoxerScore:
            print(f' {self.name} bet on BBoxer')
        else:
            print(f' {self.name} bet on ABoxer')

class BoxFight:
    def __init__(self):
        self.players = []
        self.ABoxerScore = 0
        self.BBoxerScore = 0

    def add_player(self, player):
        self.players.append(player)

    def notify(self):
        for player in self.players:
            player.update(self)
    def next_round(self):
        self.ABoxerScore += randint(0, 5)
        self.BBoxerScore += randint(0, 5)
        print(f'ABoxerScore {self.ABoxerScore}')
        print(f'BBoxerScore {self.BBoxerScore}')
        self.notify()

if __name__=='__main__':
    rp = RiskyPlayer()
    cp = ConservativePlayer()
    bf = BoxFight()
    bf.add_player(rp)
    bf.add_player(cp)

    bf.next_round()
    bf.next_round()
    bf.next_round()