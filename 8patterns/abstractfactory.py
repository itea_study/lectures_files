import random
from abc import ABC, abstractmethod


# Factories:
class AnimalForShop:
    @abstractmethod
    def speak(self): pass

class Dog(AnimalForShop):

    def speak(self):
        return "woof"

    def __str__(self):
        return "Dog"

class Cat(AnimalForShop):

    def speak(self):
        return "meow"

    def __str__(self):
        return "Cat"


class PetFactory(ABC):
    @abstractmethod
    def __call__(self, *args, **kwargs): pass

class CatFactory(PetFactory):
    def __call__(self, *args, **kwargs): return Cat()
class DogFactory(PetFactory):
    def __call__(self, *args, **kwargs): return Dog()
class RandomFactory(PetFactory):
    def __call__(self, *args, **kwargs):
        return random.choice([Dog, Cat])()

class PetShop(object):
    def __init__(self, animal_factory=None):
        self.pet_factory = animal_factory

    def show_pet(self):
        """Creates and shows a pet using the abstract factory"""

        pet = self.pet_factory()
        print("We have a lovely {}".format(pet))
        print("It says {}".format(pet.speak()))

# Show pets with various factories
if __name__ == "__main__":

    # A Shop that sells only dogs
    dogs_factory = DogFactory()
    dog_shop = PetShop(dogs_factory)
    dog_shop.show_pet()
    print("")

    # A shop that sells random animals
    random_factory = RandomFactory()
    shop = PetShop(random_factory)
    for i in range(3):
        shop.show_pet()
print("=" * 20)