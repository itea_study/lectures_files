from abc import ABC, abstractmethod
from zope.component.globalregistry import getGlobalSiteManager
from zope.interface import Interface, implementer, implements

class IContent(Interface):
    """markup interface"""

class IBelowThresholdContent(IContent):
    """markup interface"""

class IOpenUaContent(IBelowThresholdContent):
    """markup interface"""

class IConfigurator(Interface):
    """markup interface for configurators"""

@implementer(IContent)
class BaseTender(ABC):
    @abstractmethod
    def __init__(self): pass

@implementer(IBelowThresholdContent)
class BelowThreshold(BaseTender):
    def __init__(self, data):
        self.data = data

@implementer(IOpenUaContent)
class OpenUa(BelowThreshold):
    def __init__(self, data):
        self.data = data

class ContentConfigurator:
    name = "Base Configurator"
    def __init__(self, context):
        self.context = context

class BelowThresholdConfigurator(ContentConfigurator):
    name = "BelowThreshold Configurator"
    def __init__(self, context):
        self.context = context
        self.access_level = 4

class OpenUaConfigurator(BelowThresholdConfigurator):
    name = "OpenUa Configurator"
    def __init__(self, context):
        self.context = context
        self.access_level = 3

gsm = getGlobalSiteManager()
gsm.registerAdapter(ContentConfigurator, (IContent,), IConfigurator)
gsm.registerAdapter(BelowThresholdConfigurator, (IBelowThresholdContent,), IConfigurator)
gsm.registerAdapter(OpenUaConfigurator, (IOpenUaContent,), IConfigurator)

if __name__=='__main__':
    openua_tender = OpenUa(3000)
    openua_adapter = gsm.queryMultiAdapter((openua_tender,), IConfigurator)
    print(openua_adapter.access_level)
    print(openua_adapter.context.data)
    belowtheshold_tender = BelowThreshold(1000)
    belowthreshold_adapter = gsm.queryMultiAdapter((belowtheshold_tender,), IConfigurator)
    print(belowthreshold_adapter.access_level)
    print(belowthreshold_adapter.context.data)
